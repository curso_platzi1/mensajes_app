package com.mycompany.mensajes_app;

import java.util.Scanner;

public class MensajesService {
    public static void crearMensaje(){
        Scanner sc =  new Scanner(System.in);
        System.out.println("Escribe tu mensaje:");
        String mensaje = sc.nextLine();
        
        System.out.println("Tu nombre:");
        String autor = sc.nextLine();
        
        Mensajes newMensaje =  new Mensajes();
        newMensaje.setMensaje(mensaje);
        newMensaje.setAutor_mensaje(autor);
        
        MensajesDAO.crearMensajeDB(newMensaje);
    }
    
    public static void listarMensajes(){
        MensajesDAO.leerMensajesDB();
    }
    
    public static void borrarMensaje(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Indica el id del mensaje a borrar");
        int id_mensaje = sc.nextInt();
        MensajesDAO.borrarMensajesDB(id_mensaje);
    }
    
    public static void editarMensaje(){
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Escribe tu numero mensaje");
        String newMensaje = sc.nextLine();
        
        System.out.println("Ingresa el id del mensaje a modificar");
        int id = sc.nextInt();
        
        Mensajes mensaje = new Mensajes();
        mensaje.setId_mensaje(id);
        mensaje.setAutor_mensaje(newMensaje);
        
        MensajesDAO.editarMensajeDB(mensaje);
    }
}
