package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MensajesDAO {
    
    public static void crearMensajeDB(Mensajes mensaje){
        Conexion db_conect = new Conexion();
        try(Connection conexion = db_conect.getConnection()){
            PreparedStatement ps = null;
            try{
                String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?,?)";
                
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje creado.");
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void leerMensajesDB(){
        Conexion db_conect = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try(Connection conexion = db_conect.getConnection()){
            String query = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();
            
            while (rs.next()){
                System.out.println("ID: " + rs.getInt("id_mensaje"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha publicacion: " + rs.getString("fecha_mensaje"));
                System.out.println("");
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void borrarMensajesDB(int id){
        Conexion db_conect = new Conexion();
        
        try(Connection conexion = db_conect.getConnection()){
            PreparedStatement ps = null;
            try{
                String query = "DELETE FROM mensajes WHERE mensajes.id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1,id);
                ps.executeUpdate();
                System.out.println("Mensaje borrado");
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
    
    public static void editarMensajeDB(Mensajes mensaje){
        Conexion db_conect = new Conexion();
        
        try(Connection conexion = db_conect.getConnection()){
            PreparedStatement ps = null;
            
            try{
                String query = "UPDATE mensajes SET mensaje = ? WHERE mensajes.id_mensaje = ?";
                ps = conexion.prepareCall(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2,mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println("El mensaje a sido actualizado.");
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
}
